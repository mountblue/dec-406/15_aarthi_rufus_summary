package com.tanza.rufus.api;


public class Summary
{
    public String getSm_api_character_count() {
        return sm_api_character_count;
    }

    public void setSm_api_character_count(String sm_api_character_count) {
        this.sm_api_character_count = sm_api_character_count;
    }

    public String getSm_api_content_reduced() {
        return sm_api_content_reduced;
    }

    public void setSm_api_content_reduced(String sm_api_content_reduced) {
        this.sm_api_content_reduced = sm_api_content_reduced;
    }

    public String getSm_api_title() {
        return sm_api_title;
    }

    public void setSm_api_title(String sm_api_title) {
        this.sm_api_title = sm_api_title;
    }

    public String getSm_api_content() {
        return sm_api_content;
    }

    public void setSm_api_content(String sm_api_content) {
        this.sm_api_content = sm_api_content;
    }

    public String getSm_api_limitation() {
        return sm_api_limitation;
    }

    public void setSm_api_limitation(String sm_api_limitation) {
        this.sm_api_limitation = sm_api_limitation;
    }

    private String sm_api_character_count;
    private String sm_api_content_reduced;
    private String sm_api_title;
    private String sm_api_content;
    private String sm_api_limitation;

    public Summary(String sm_api_character_count, String sm_api_content_reduced, String sm_api_title,
                   String sm_api_content, String sm_api_limitation)
    {
        this.sm_api_character_count = sm_api_character_count;
        this.sm_api_content_reduced = sm_api_content_reduced;
        this.sm_api_title = sm_api_title;
        this.sm_api_content = sm_api_content;
        this.sm_api_limitation = sm_api_limitation;
    }

    public Summary(){}
}
