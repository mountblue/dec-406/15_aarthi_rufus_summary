package com.tanza.rufus.resources;

import com.tanza.rufus.api.Summary;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;


@Path("/Summary")
@Produces(MediaType.APPLICATION_JSON)
public class SummaryResource {

    private Client client;

    private String apiURL;
    private String SM_API_KEY;


    private static final String redisHost = "localhost";
    private static final Integer redisPort = 6379;

    //the jedis connection pool..
    private static JedisPool pool = null;

    public SummaryResource(Client client, String apiURL, String SM_API_KEY, String SM_URL) {

        this.client = client;
        this.apiURL = apiURL;
        this.SM_API_KEY = SM_API_KEY;
        pool = new JedisPool(redisHost, redisPort);
    }

    public String getCachedSummary(String url) {
        Jedis jedis = this.pool.getResource();
        if (!jedis.exists(url)) {
            return null;
        }
        return jedis.get(url);
    }

    public void cacheSummary(String url, String summary) {
        Jedis jedis = this.pool.getResource();
        jedis.set(url, summary);
    }

    public String summarizeURL(String url) {
        Summary summary = client
                .target(apiURL)
                .queryParam("SM_API_KEY", SM_API_KEY).queryParam("SM_URL", url)
                .request(MediaType.APPLICATION_JSON)
                .get(Summary.class);
        return summary.getSm_api_content();
    }

    @GET
    public String summary(@Context UriInfo info) {
        String url = info.getQueryParameters().getFirst("url");

        String summary = this.getCachedSummary(url);
        if (summary != null) {
            return summary;
        }
        summary = this.summarizeURL(url);
        this.cacheSummary(url, summary);
        return summary;
    }
}